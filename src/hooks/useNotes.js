import { useReducer } from 'react';
import { notesReducer } from './notesReducer';

const initialState = [];

export const useNotes = () => {
	const notesInit = () => {
		return JSON.parse(localStorage.getItem('easy-notes')) || initialState;
	};

	const [notes, dispatch] = useReducer(notesReducer, [], notesInit);

	const addNote = (note) => {
		const action = {
			type: 'ADD',
			payload: { ...note, id: new Date().getTime() },
		};

		dispatch(action);
	};

	const editNote = (note) => {
		const action = {
			type: 'EDIT',
			payload: note,
		};

		dispatch(action);
	};

	const removeNote = (noteId) => {
		const action = {
			type: 'DEL',
			payload: noteId,
		};

		dispatch(action);
	};

	return { notes, addNote, editNote, removeNote };
};
