export const notesReducer = (state, action) => {
	const { type, payload } = action;

	switch (type) {
		case 'ADD':
			return [payload, ...state];

		case 'EDIT':
			return state.map((note) => (note.id === payload.id ? payload : note));

		case 'DEL':
			return state.filter(({ id }) => id !== payload);

		default:
			return state;
	}
};
