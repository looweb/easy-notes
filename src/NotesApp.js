import { useEffect, useState } from 'react';
import { Button, Container, Group, MantineProvider, Text, useMantineTheme } from '@mantine/core';
import { useModals } from '@mantine/modals';

import { useNotes } from 'hooks/useNotes';
import Header from 'components/Header';
import NotesList from 'components/NotesList';
import EditNoteModal from 'components/EditNoteModal';

import 'components/components.scss';
import './NotesApp.scss';
import NoteModal from 'components/NoteModal';
import Footer from 'components/Footer';
import { Plus } from 'tabler-icons-react';

const NotesApp = () => {
	const [showNoteForm, setShowNoteForm] = useState(false);
	const [selectedNote, setSelectedNote] = useState(null);
	const [selectedForView, setSelectedForView] = useState(null);
	const { colors } = useMantineTheme();
	const { notes, addNote, editNote, removeNote } = useNotes();
	const modals = useModals();

	useEffect(() => {
		localStorage.setItem('easy-notes', JSON.stringify(notes));
	}, [notes]);

	const handleShowNoteForm = () => setShowNoteForm(true);
	const handleCloseNoteForm = () => {
		setShowNoteForm(false);
		if (selectedNote) setSelectedNote(null);
	};
	const handleClickEditNote = (note) => {
		setSelectedNote(note);
		handleShowNoteForm();
	};

	const handleClickEditFromModal = (note) => {
		handleClickEditNote(note);
		setSelectedForView(false);
	};

	const handleSubmit = (values) => {
		const action = selectedNote ? editNote : addNote;
		action(selectedNote ? { ...values, id: selectedNote.id } : values);
		handleCloseNoteForm();
	};

	const handleDeleteNote = (noteId) => removeNote(noteId);

	const confirmDeleteNote = (note) => {
		if (selectedForView) setSelectedForView(null);

		modals.openConfirmModal({
			title: <Text style={{ fontWeight: 700 }}>Confirme</Text>,
			children: (
				<Text>
					¿Realmente desea eliminar "<span style={{ fontWeight: 500 }}>{note.title}</span>"
					permanentemente?
				</Text>
			),
			labels: { confirm: 'Sí, borrar', cancel: 'Cancelar' },
			confirmProps: {
				color: 'red',
			},
			onCancel: () => {},
			onConfirm: () => handleDeleteNote(note.id),
		});
	};

	const handleClickView = (note) => setSelectedForView(note);

	return (
		<MantineProvider
			theme={{
				headings: {
					fontFamily: 'Arial, Helvetica, sans-serif',
				},
			}}
		>
			<div className="NotesApp">
				<div className="NotesApp-header" style={{ backgroundColor: colors.gray[8] }}>
					<Container size="xl">
						<Header />
					</Container>
				</div>
				<div className="NotesApp-body">
					<Container size="xl">
						<Group position="right">
							<Button onClick={handleShowNoteForm} leftIcon={<Plus size={16} />}>
								Añadir nota
							</Button>
						</Group>
						<NotesList
							notes={notes}
							onClickEdit={handleClickEditNote}
							onClickDelete={confirmDeleteNote}
							onClickView={handleClickView}
						/>
					</Container>
				</div>
				<div className="NotesApp-footer" style={{ backgroundColor: colors.gray[3] }}>
					<Container size="xl">
						<Footer />
					</Container>
				</div>
			</div>
			<EditNoteModal
				note={selectedNote}
				onClose={handleCloseNoteForm}
				onSubmit={handleSubmit}
				opened={showNoteForm}
			/>

			<NoteModal
				note={selectedForView}
				onClickDelete={confirmDeleteNote}
				onClickEditNote={handleClickEditFromModal}
				onClose={() => setSelectedForView(null)}
				opened={!!selectedForView}
			/>
		</MantineProvider>
	);
};

export default NotesApp;
