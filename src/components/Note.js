import { ActionIcon, Card, Group, Text, Title } from '@mantine/core';
import { Edit, Eye, Trash } from 'tabler-icons-react';
import Tools from 'utils/Tools';

const { countWords, getFirstWords, replaceLineBreak, getDateTime } = Tools;
const maxWords = 15;

const Note = ({ note, onClickEdit, onClickDelete, onClickView }) => {
	const { id, title, text } = note;

	let noteText = text;
	const isTooLong = countWords(noteText) > maxWords;

	if (isTooLong) {
		noteText = `${getFirstWords(noteText, maxWords)}...`;
	}

	return (
		<Card key={id} withBorder shadow="md" className="Note">
			<Title order={2} sx={() => ({ fontSize: 18 })}>
				{title}
			</Title>
			<Text className="Note-text" size="sm" sx={({ colors }) => ({ color: colors.gray[7] })}>
				<div dangerouslySetInnerHTML={{ __html: replaceLineBreak(noteText) }} />
				<Text size="xs" color="dimmed" mt={8} align="right">
					{getDateTime(id)}
				</Text>
			</Text>

			<Group align="center" position="right">
				<ActionIcon
					color="red"
					size="sm"
					sx={{ marginRight: 'auto' }}
					onClick={() => onClickDelete(note)}
				>
					<Trash />
				</ActionIcon>
				<ActionIcon color="blue" size="md" onClick={() => onClickEdit(note)}>
					<Edit />
				</ActionIcon>
				{isTooLong && (
					<ActionIcon color="gray" size="md" onClick={() => onClickView(note)}>
						<Eye />
					</ActionIcon>
				)}
			</Group>
		</Card>
	);
};

export default Note;
