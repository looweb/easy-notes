import { Modal } from '@mantine/core';

import ModalTitle from './ModalTitle';
import NoteForm from './NoteForm';

const EditNoteModal = ({ onClose, note, onSubmit, opened }) => {
	const title = note ? 'Editar nota' : 'Nueva nota';

	return (
		<Modal
			transition="pop"
			transitionDuration={350}
			title={<ModalTitle>{title}</ModalTitle>}
			overflow="outside"
			overlayOpacity={0.5}
			opened={opened}
			onClose={onClose}
			closeOnClickOutside={false}
			size={485}
		>
			{opened && <NoteForm note={note} onSubmit={onSubmit} onCancel={onClose} />}
		</Modal>
	);
};

export default EditNoteModal;
