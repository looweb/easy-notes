import { useState } from 'react';

import { Button, Title } from '@mantine/core';
import ChangeLogModal from './ChangeLogModal';

const Header = () => {
	const [showAbout, setShowAbout] = useState(false);

	return (
		<div className="Header">
			<Title
				sx={({ colors }) => ({
					color: colors.gray[0],
					fontSize: 26,
				})}
			>
				Easy Notes
			</Title>
			<Button color="gray" onClick={() => setShowAbout(true)}>
				Acerca de
			</Button>
			<ChangeLogModal isOpen={showAbout} onClose={() => setShowAbout(false)} />
		</div>
	);
};

export default Header;
