import { Button, Divider, Group, Modal, Text } from '@mantine/core';
import Tools from 'utils/Tools';
import ModalTitle from './ModalTitle';

const NoteModal = ({ note, opened, onClose, onClickEditNote, onClickDelete }) => {
	const { title, text, id } = note || {};

	return (
		<Modal
			className="NoteModal"
			onClose={onClose}
			opened={opened}
			overflow="outside"
			overlayOpacity={0.5}
			title={<ModalTitle>{title}</ModalTitle>}
			transition="scale"
		>
			<Group direction="column" grow spacing="xs">
				{opened && (
					<Text className="NoteModal-text">
						<div dangerouslySetInnerHTML={{ __html: Tools.replaceLineBreak(text) }} />
						<Text size="sm" color="dimmed" align="right">
							{Tools.getDateTime(id)}
						</Text>
					</Text>
				)}

				<Divider my={2} />
				<Group position="right" spacing="xs">
					<Button variant="subtle" color="red" onClick={() => onClickDelete(note)}>
						Eliminar
					</Button>
					<Button
						variant="outline"
						onClick={() => onClickEditNote(note)}
						sx={{ marginRight: 'auto' }}
					>
						Editar
					</Button>
					<Button onClick={onClose}>Listo</Button>
				</Group>
			</Group>
		</Modal>
	);
};

export default NoteModal;
