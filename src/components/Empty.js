import { Alert } from '@mantine/core';
import { AlertCircle } from 'tabler-icons-react';

const Empty = ({ title, text }) => {
	return (
		<Alert icon={<AlertCircle size={22} />} title={title} variant="outline">
			{text}
		</Alert>
	);
};

export default Empty;
