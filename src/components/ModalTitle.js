import { Text } from '@mantine/core';

const ModalTitle = ({ children }) => {
	return <Text style={{ fontWeight: 700, fontSize: 18 }}>{children}</Text>;
};

export default ModalTitle;
