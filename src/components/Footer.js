import { Anchor, Text } from '@mantine/core';
import React from 'react';

const Footer = () => {
	return (
		<div className="Footer">
			<Text style={{ fontWeight: 500 }}>
				Desarrollado por{' '}
				<Anchor href="https://loodesarrollos.com" target="_blank">
					Luis Suárez
				</Anchor>
			</Text>
		</div>
	);
};

export default Footer;
