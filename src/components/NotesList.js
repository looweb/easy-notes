import Empty from './Empty';
import Note from './Note';

const NotesList = ({ notes, onClickEdit, onClickDelete, onClickView }) => {
	const isEmpty = !notes?.length;

	return (
		<div className="NotesList">
			{isEmpty ? (
				<Empty
					title="No tienes notas"
					text={
						<>
							No tienes notas guardadas aún. Haz click en <strong>Añadir Nota</strong> para crear
							una nueva.
						</>
					}
				/>
			) : (
				<div className="NotesList-list">
					{notes.map((note) => {
						return (
							<div className="NotesList-list-item" key={note.id}>
								<Note
									note={note}
									onClickEdit={onClickEdit}
									onClickDelete={onClickDelete}
									onClickView={onClickView}
								/>
							</div>
						);
					})}
				</div>
			)}
		</div>
	);
};

export default NotesList;
