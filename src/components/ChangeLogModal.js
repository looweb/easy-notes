import { List, Modal, Text, ThemeIcon, Title } from '@mantine/core';
import { Bug, LayoutGridAdd, Palette, Plus, Rocket } from 'tabler-icons-react';

import ModalTitle from './ModalTitle';

const { Item: ListItem } = List;

const icons = {
	bugfix: <Bug />,
	improve: <Rocket />,
	new: <Plus />,
	ui: <Palette />,
	minor: <LayoutGridAdd />,
};

const iconColor = {
	bugfix: 'red',
	improve: 'blue',
	new: 'green',
	ui: 'indigo',
	minor: 'cyan',
};

const changeLog = [
	{
		description: 'Versión 1.',
		id: '2022-03-27',
		type: 'new',
		version: '1.0',
	},
	{
		description: 'Se añade registro de cambios.',
		id: '2022-03-29',
		type: 'minor',
		version: '1.0.1',
	},
	{
		description: 'Editor de texto enriquecido al crear/editar la nota.',
		id: '2022-03-30',
		type: 'new',
		version: '1.1',
	},
].reverse();

const ChangeLogModal = ({ onClose, isOpen }) => {
	return (
		<Modal opened={isOpen} title={<ModalTitle>Acerca de Easy Notes</ModalTitle>} onClose={onClose}>
			<Text>
				<strong>Easy notes</strong> es una aplicación para mantener notas sencillas en el navegador
				de tu preferencia.
			</Text>
			<Title order={2} style={{ fontSize: 16, marginTop: 16 }}>
				Historial de cambios
			</Title>
			<List size="md" spacing="xs" mt={4}>
				{changeLog.map(({ id, version, description, type }) => (
					<ListItem
						key={id}
						icon={
							<ThemeIcon color={iconColor[type]} size={18} mt={5}>
								{icons[type]}
							</ThemeIcon>
						}
					>
						<Text weight={500}>
							{version} <small>({new Date(id).toLocaleDateString('es-VE')})</small>
						</Text>
						<Text size="sm">{description}</Text>
					</ListItem>
				))}
			</List>
		</Modal>
	);
};

export default ChangeLogModal;
