import { Button, Group, InputWrapper, TextInput } from '@mantine/core';
import { useFormik } from 'formik';
import { RichTextEditor } from '@mantine/rte';
import { validationSchemas } from 'utils/validationSchemas';

const NoteForm = ({ note, onSubmit, onCancel }) => {
	const formik = useFormik({
		initialValues: {
			title: note?.title || '',
			text: note?.text || '',
		},
		validationSchema: validationSchemas.note,
		onSubmit,
	});

	return (
		<form onSubmit={formik.handleSubmit}>
			<Group direction="column" grow>
				<TextInput
					error={formik.touched.title && Boolean(formik.errors.title) && formik.errors.title}
					label="Título"
					name="title"
					onChange={formik.handleChange}
					value={formik.values.title}
					size="md"
				/>
				<InputWrapper
					error={formik.touched.text && Boolean(formik.errors.text) && formik.errors.text}
				>
					<RichTextEditor
						styles={() => ({ toolbarGroup: { margin: 4 } })}
						onChange={(text) => (formik.values.text = text)}
						value={formik.values.text}
						controls={[
							['bold', 'italic', 'underline', 'strike', 'link'],
							['unorderedList', 'h1', 'h2', 'h3'],
							['alignLeft', 'alignCenter', 'alignRight'],
							['code', 'codeBlock'],
						]}
					/>
				</InputWrapper>
				<Group position="right">
					<Button variant="outline" onClick={onCancel}>
						Cancelar
					</Button>
					<Button type="submit">{note ? 'Guardar' : 'Crear nota'}</Button>
				</Group>
			</Group>
		</form>
	);
};

export default NoteForm;
