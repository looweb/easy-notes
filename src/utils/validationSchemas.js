import * as yup from 'yup';

export const validationSchemas = {
	note: yup.object({
		title: yup.string().required('Escribe un título para tu nota'),
		text: yup.string().required('No olvides escribir algo...'),
	}),
};
