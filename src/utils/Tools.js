export default class Tools {
	static replaceLineBreak(text) {
		return text.replace(/(?:\r\n|\r|\n)/g, '<br />');
	}

	static countWords(text) {
		return text.trim().split(' ').length;
	}

	static getFirstWords(text, n = 20) {
		if (Tools.countWords(text) > n) {
			return text
				.trim()
				.split(' ')
				.slice(0, n - 1)
				.join(' ');
		}

		return text;
	}

	static getDateTime(dateTime) {
		return `${new Date(dateTime).toLocaleDateString('es-VE')} ${new Date(
			dateTime
		).toLocaleTimeString('es-VE')}`;
	}
}
